package test;

import entidades.Facturas;
import java.util.ArrayList;

public class TestFacturas {

    public static void main(String[] args) {
        System.out.println("[...]");
        ArrayList<Facturas> listado = new ArrayList();
        Facturas miFactu1 = new Facturas("Churro", 10);
        Facturas miFactu2 = new Facturas("Bola de fraile", 25);
        Facturas miFactu3 = new Facturas("Medialuna", 15);
        listado.add(miFactu1);
        listado.add(miFactu2);
        listado.add(miFactu3);

        try {
            for (int i = 0; i < 4; i++) {
                System.out.println(listado.get(i).getNombre());
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Aprende a programar! " + e.getMessage());
        }

        System.out.println("[OK]");
    }
}
