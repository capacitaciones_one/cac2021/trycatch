package entidades;

public class Facturas {

    int precio;
    String nombre;

    public Facturas(String nombre, int precio) {
        setNombre(nombre);
        setPrecio(precio);
        
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

   
}
